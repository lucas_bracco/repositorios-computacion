Proceso Matriz_Traspuesta
	Definir i,j,n,matriz Como Entero;
	Dimension matriz[3,3];
	n<-1;
	Para i<-0 hasta 2 Con Paso 1 Hacer
		para j<-0 hasta 2 Con Paso 1 hacer
			matriz[i,j]<-n;
			n<-n+1;
		FinPara
	FinPara
	Escribir "Matriz Original:";
	Para i<-0 hasta 2 Con Paso 1 Hacer
		para j<-0 hasta 2 Con Paso 1 Hacer
			Escribir Sin Saltar "|",matriz[i,j],"|";
		FinPara
		Escribir "";
	FinPara
	Escribir "";
	Escribir "Matriz Traspuesta:";
	Para i<-0 hasta 2 Con Paso 1 Hacer
		para j<-0 hasta 2 Con Paso 1 Hacer
			Escribir Sin Saltar "|",matriz[j,i],"|";
		FinPara
		Escribir "";
	FinPara
FinProceso
