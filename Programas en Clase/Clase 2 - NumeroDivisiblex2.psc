Algoritmo ejemploClase
	
	i=0
	Para i<-0 Hasta 10 Con Paso 1 Hacer
		
		Si i/2=0 Entonces
			Escribir 'El numero ' i ' es divisible por 2'
		SiNo
			Escribir 'El numero ' i ' NO es divisible por 2'
		Fin Si
	Fin Para
FinAlgoritmo
