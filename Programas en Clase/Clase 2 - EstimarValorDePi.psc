Algoritmo EstimarValorPi
	
	Definir i,n,aux Como Entero;
	
	Definir auxpar,auximpar,r Como Real;
	
	Escribir 'Ingrese el valor de n';
	
	Leer n;
	
	auxpar <- 0;
	
	auximpar <- 0;
	
	aux <- 1;
	
	Para i<-0 Hasta n Con Paso 1 Hacer
		
		Si i mod 2 = 0 Entonces
			
			auxpar <- auxpar + (1/aux);
			
		SiNo
			
			auximpar <- auximpar + (1/aux);
			
		FinSi
		
		aux <- aux+2;
		
	FinPara
	
	r <- 4*(auxpar-auximpar);
	
	Escribir "El valor de pi es: ", r;
	
FinAlgoritmo