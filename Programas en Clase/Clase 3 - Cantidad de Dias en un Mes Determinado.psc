Funcion es_bisiesto <- bisiesto(anio)
	Definir es_bisiesto Como Logico;
	si anio%100=0
		Entonces
		es_bisiesto<-Falso;
	SiNo
		si anio%4=0
			Entonces
			es_bisiesto<-Verdadero;
		FinSi
	FinSi
FinFuncion

	Proceso Dias_Mes
	Definir mes,anio Como Entero;
	Escribir "Introduzca un Mes (En n�mero)";
	Leer mes;
	Segun mes hacer
		1,3,5,7,8,10,12:
			Escribir "31 d�as";
		2:
			Escribir "Ingrese el A�o";
			Leer anio;
			si bisiesto(anio)=Verdadero Entonces
				Escribir "29 d�as";
			SiNo
				Escribir "28 d�as";
			FinSi
		4,6,9,11:
			Escribir "30 d�as";
		De Otro Modo:
			Escribir "Mes no v�lido";
	FinSegun
FinProceso
